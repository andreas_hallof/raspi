# Server-Upload-Verzeichnis

Hier speichert der Server die hochgeladenen Daten.
Pro User gibt es ein Verzeichnis.
Pro Upload-Daten gibt es ein Unterverzeichnis darin (Nutzer-spezifisch).
In solch einen Unterverzeichnis werden die einzelnen Blöcke und der TOC 
gespeichert.

