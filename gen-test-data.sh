#! /bin/bash

if [ "$1" = "" ]; then
    dd if=/dev/urandom of=test-data.bin bs=1024 count=10
elif [ "$1" = zero ]; then
    dd if=/dev/urandom of=test-data.bin bs=1024 count=10
elif [ "$1" = a ]; then
    python -c 'print("a"*10240, end="")' >test-data.bin
fi

