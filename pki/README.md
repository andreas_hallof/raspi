# Usermanagement

Im Sicherheitsbereich -- also nicht auf dem Server -- werden 
Signaturschlüsselpaare für die einzelnen Nutzer erzeugt.
Diese müssen sicher an die Nutzer verteilt werden.

# Verschlüsselungsschlüssel für den Sicherheitsbereich

Im Sicherheitsbereich wird ein Verschlüsselungsschlüsselpaar erzeugt.
Der öffentliche Schlüssel des Paars wird in den Client festverdrahtet.
Der Client verschlüsselt die hochzuladenen Daten mit diesem öffentlichen
Schlüssel. -> Schutzbedarf auf dem Server minimiert.

