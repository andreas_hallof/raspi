#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import ec

from base64 import b64encode

user_db=[]

for username in ['ava', 'bob', 'cera', 'dan']:
    private_key = ec.generate_private_key(
                    ec.SECP256R1(),
                    default_backend()
                )
    with open(username + "-key.pem", "wt") as f:
        f.write(
            private_key.private_bytes(
                serialization.Encoding.PEM,
                serialization.PrivateFormat.PKCS8,
                serialization.NoEncryption()
            ).decode()
        )

    public_key = private_key.public_key()
    p_der_base64 = b64encode(
                    public_key.public_bytes(
                        serialization.Encoding.DER,
                        serialization.PublicFormat.SubjectPublicKeyInfo
                    )
                  ).decode()
    print(username, p_der_base64)
    user_db.append([username, p_der_base64])


if user_db:
    with open("user_db.txt", "wt") as f:
        for username, p_der_base64 in user_db:
            f.write(username + " " + p_der_base64 + "\n")


