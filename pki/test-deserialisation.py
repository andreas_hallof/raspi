#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import secrets, sys, time

from base64 import b64encode, b64decode
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


server_freshness_secret = secrets.token_bytes(16)
server_token_secret = secrets.token_bytes(16)

B64_FRESHNESS_LENGTH = 52
USER_DB = "user_db.txt"
MAX_TIME_DIFF = 120
MAX_TOKEN_AGE = 7*24*60*60

if not os.path.exists(USER_DB):
    sys.exit(USER_DB + " not found")

# Ich erzeuge eine Frischeinformation für das folgende Challenge-Response
assert server_freshness_secret
iv = secrets.token_bytes(12)
pt = "{}".format(int(time.time())).encode()
iv_plus_ct = iv + AESGCM(server_freshness_secret).encrypt(iv, pt, b'')

current_freshness = b64encode(iv_plus_ct).decode()
assert len(current_freshness) == B64_FRESHNESS_LENGTH

# jetzt erzeuge ich als Client eine Signatur dafür

# jetzt prüfe ich die Signatur und die Frischeinformationen
user_db_dict = dict()
with open(USER_DB, "rt") as user_db_file:
    for entry in user_db_file.readlines():
        (username, pub_key) = entry.split()
        user_db_dict[pub_key] = username

if not UserPublicKey in user_db_dict:
    sys.exit("not a valid user public key")

(prefix, freshness) = dtbs.split()
if prefix != "freshness:":
    raise HTTPException(status_code=400, detail="dtbs wrong structure 1")
if len(freshness) != B64_FRESHNESS_LENGTH:
    raise HTTPException(status_code=400, detail="Freshness len check FAIL")

try:
    f_decode = b64decode(freshness)
    plaintext = AESGCM(server_freshness_secret).decrypt(
        f_decode[0:12], f_decode[12:], associated_data=None)
    f_time = int(plaintext)
    if abs(time.time()-f_time) > MAX_TIME_DIFF:
        raise HTTPException(status_code=400, detail="freshness too old")

    pub_key = xxx
    pub_key.verify(Signature, dtbs.encode(), ec.ECDSA(hashes.SHA256()))

    # xxx signatur prüfen

except ValueError:
    raise HTTPException(status_code=400, detail="dtbs wrong structure 2")
except InvalidTag:
    raise HTTPException(status_code=400, detail="decrypt error")
except InvalidSignature:
    raise HTTPException(status_code=400, detail="signature verification failed")
except:
    raise HTTPException(status_code=400, detail="dtbs wrong structure xxx")

new_token_pt = "token created: {} username: {}".format(
    int(time.time()), user_db_dict[UserPublicKey])
iv = secrets.token_bytes(12)
new_token = iv + AESGCM(server_token_secret).encrypt(iv, new_token_pt, b'')
new_token = b64encode(new_token).decode()

return {"Usertoken" : new_token}

