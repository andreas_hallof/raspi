#! /bin/bash

# diese Datei muss man per 'source' oder '.' in der aktuell
# laufenden bash-Instanz source-n

function d() {

   if [ "$1" = "all" ]; then 
       for i in 0_ServerData/anon/*; do
          echo $i
          rm -frv $i
       done
   elif [ "$1" = "" ]; then
cat <<EOF
Löscht Daten aus dem Server-Storage:
        bei Argument "all" löscht er alle Dateien
        ansonsten alle Fragmente die mit /arg1 beginnen
EOF
   else
       find 0_ServerData/anon -type f | grep -v TOC | pcregrep "/$1[^/]+$" | while read a; do
        echo rm $(basename $a)
        rm $a
       done
   fi
}

