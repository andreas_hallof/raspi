# Nach UTF-8 Umkodierung

iconv für latin1-\>utf8 Umkodierungen:
https://www.tecmint.com/convert-files-to-utf-8-encoding-in-linux/

    $ file -i input.file
    $ iconv -f ISO-8859-1 -t UTF-8//TRANSLIT input.file -o out.file

