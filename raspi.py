#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import glob, hashlib, json, os, re, secrets, sys, time

from base64 import b64encode, b64decode
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

from fastapi import FastAPI, Header, HTTPException, Request
#from pydantic import BaseModel
from typing import ClassVar, List, Optional

#class HashValueType(str):
#    strip_whitespace: ClassVar[bool] = True
#    min_length: ClassVar[int] = 64
#    max_length: ClassVar[int] = 64
#    my_data: str

tags_metadata = [
    {"name": "Status",
     "description" : \
     """Hiermit kann ein Client die vom Server verwendete API-Version erfahren.
    Aber noch wichtiger ist, dass der Client damit prüft, ob seine 
    Proxy-Einstellungen OK sind und er den Server überhaupt erreichen
    kann.
    """},

    {"name": "UserToken",
     "description" : \
     """Der Nutzer bezieht per GET-Request einen Zufallswert, er signiert diesen
      und erhält nach Prüfung durch den Server ein Authentisierungstoken.
      Diese Authentisierungstoken wird anschließend bei den folgenden 
      Upload-Request vom Client mit gesendet.
      """},
    {"name": "TOC",
     "description" : """Ein array von Hashwerten (der Datenblöcke) als
                      TableOfContents hochladen."""},
    {"name": "HashesOfMissingBlocks",
     "description" : """Gibt die Hashwerte der noch fehlenden Blöcke zurück.
      Falls noch kein TOC hochgeladen wurde dann kommt ["all"] zurück."""},
    {"name": "PushBlock",
      "description" : "Lädt einen Datenblock hoch (application/octet-steam)."}
     ]

app = FastAPI(
        title="raspi",
        description="Kleines Projekt, große Dateien auf einen HTTPS-Server hochladen.",
        version="1",
        openapi_tags=tags_metadata)

server_freshness_secret = secrets.token_bytes(16)
server_token_secret = secrets.token_bytes(16)
B64_FRESHNESS_LENGTH = 52
DATA_DIR = "0_ServerData"
MAX_BLOCKS = 500
MIN_BLOCK_SIZE = 1024
MAX_BLOCK_SIZE = 4194304
USER_DB = "user_db.txt"
MAX_TIME_DIFF = 120
MAX_TOKEN_AGE = 7*24*60*60

if not os.path.exists(USER_DB):
    sys.exit(USER_DB + " not found")

@app.get("/Status", tags=["Status"])
def get_Status():
    """
    Das gibt die vom Server verwendete API-Version zurück.

    Aber noch wichtiger ist, dass der Client damit prüft, ob seine 
    Proxy-Einstellungen OK sind und er den Server überhaupt erreichen
    kann.
    """
    return {"Status" : "OK", "Version" : "1"}

@app.get("/UserToken", tags=["UserToken"])
def get_UserToken():
    """
    Gibt die aktuelle Zeit symmertrisch verschlüsselt als Freshness zurück.
    Diese wird beim folgenden POST-Request wieder entschlüsselt und damit
    überprüft, ob die Signatur (Authentisierung) nicht schon zu alt ist.
    """
    assert server_freshness_secret
    iv = secrets.token_bytes(12)
    pt = "{}".format(int(time.time())).encode()
    iv_plus_ct = iv + AESGCM(server_freshness_secret).encrypt(iv, pt, b'')

    ret = b64encode(iv_plus_ct).decode()
    assert len(ret) == B64_FRESHNESS_LENGTH

    return ret

# Hier muss man auf die Duplicate Signature Key Selection Attack
# https://www.agwa.name/blog/post/duplicate_signature_key_selection_attack_in_lets_encrypt
# achten. Daher wir explizit der öffentliche Schlüssel als "UserID" 
# übertragen.
@app.post("/UserToken", tags=["UserToken"])
def post_UserToken(dtbs: str, UserPublicKey: str, Siganture: str):
    """
    xxx
    """
    user_db_dict = dict()
    with open(USER_DB, "rt") as user_db_file:
        for entry in user_db_file.readlines():
            (username, pub_key) = entry.split()
            user_db_dict[pub_key] = username

    if not UserPublicKey in user_db_dict:
        raise HTTPException(status_code=400, detail="not a valid user public key")

    (prefix, freshness) = dtbs.split()
    if prefix != "freshness:":
        raise HTTPException(status_code=400, detail="dtbs wrong structure 1")
    if len(freshness) != B64_FRESHNESS_LENGTH:
        raise HTTPException(status_code=400, detail="Freshness len check FAIL")

    try:
        f_decode = b64decode(freshness)
        plaintext = AESGCM(server_freshness_secret).decrypt(
            f_decode[0:12], f_decode[12:], associated_data=None)
        f_time = int(plaintext)
        if abs(time.time()-f_time) > MAX_TIME_DIFF:
            raise HTTPException(status_code=400, detail="freshness too old")

        pub_key = xxx
        pub_key.verify(Signature, dtbs.encode(), ec.ECDSA(hashes.SHA256()))

        # xxx signatur prüfen

    except ValueError:
        raise HTTPException(status_code=400, detail="dtbs wrong structure 2")
    except InvalidTag:
        raise HTTPException(status_code=400, detail="decrypt error")
    except InvalidSignature:
        raise HTTPException(status_code=400, detail="signature verification failed")
    except:
        raise HTTPException(status_code=400, detail="dtbs wrong structure xxx")

    new_token_pt = "token created: {} username: {}".format(
        int(time.time()), user_db_dict[UserPublicKey])
    iv = secrets.token_bytes(12)
    new_token = iv + AESGCM(server_token_secret).encrypt(iv, new_token_pt, b'')
    new_token = b64encode(new_token).decode()

    return {"Usertoken" : new_token}

def my_check_input(hashvalue_to_check: str, blocksize_to_check: int,
                   usertoken_to_check: str):
    """
    Ein Hashwert muss der Form:
        faa9153bd4e9709f47a12c708a97fa7e784130591c9ce5367ff2cd59fbf700fd
    sein.

    Die Blockgröße muss zwischen 2^10 und 2^22 (4 MiB) liegen.

    Wenn einer der zu prüfenden Werte ein FAIL ergibt, wird eine
    HTTPException erzeugt.

    """
    if not re.match(r"[0-9a-f]{64}", hashvalue_to_check):
        raise HTTPException(status_code=400, detail="not valid hash value")

    if blocksize_to_check < MIN_BLOCK_SIZE or blocksize_to_check > MAX_BLOCK_SIZE:
        raise HTTPException(status_code=400, detail="Blocksize must be in [2^10, 2^22]")

    if not re.match(r"[0-9a-z]{1,64}", usertoken_to_check):
        raise HTTPException(status_code=400, detail="not valid user_id")

    return True

def my_check_hash_array(hash_array_to_check: []):
    """
    xxx
    """

    if not hash_array_to_check:
        raise HTTPException(status_code=400, detail="not valid hash array")

    for hash_value in hash_array_to_check:
        if not re.match(r"[0-9a-z]{64}", hash_value):
            raise HTTPException(
                    status_code=400,
                    detail="not valid hash value ({}) in hash array".format(hash_value))

    return True


def my_check_usertoken(mytoken: str):
    """
    xxx
    """

    if not mytoken or mytoken == "anon":
        return "anon"

    try:
        token_decode = b64decode(mytoken)
        plaintext = AESGCM(server_token_secret).decrypt(
            token_decode[0:12],
            token_decode[12:],
            associated_data=None).decode()
        token_elements = plaintext.split()
        #"token created: {} username: {}".format(
        assert token_elements[0] == "token"
        assert token_elements[1] == "created:"
        token_created_time = int(token_elements[2])
        assert token_elements[3] == "username:"
        username = token_elements[4]

        if abs(time.time()-token_created_time) > MAX_TOKEN_AGE:
            raise HTTPException(status_code=403, detail="token too old")

    except ValueError:
        raise HTTPException(status_code=400, detail="base64-decoding err")
    except InvalidTag:
        raise HTTPException(status_code=400, detail="decrypt error")
    except:
        raise HTTPException(status_code=403, detail="invalid usertoken")

    return username

def my_check_free_server_diskspace():
    """
    xxx
    """

    #raise HTTPException(status_code=500, detail="server disk limit reached")

    return True


# Interessantes Video über post vs. put:
# https://www.youtube.com/watch?v=rhTkRK53XdQ 

@app.put("/TOC/{HashValue}/{Blocksize}", tags=["TOC"])
def put_PushTOC(
        HashValue: str,
        Blocksize: int,
        hash_array: List[str],
        Usertoken: Optional[str] = Header("anon")):
    """
    xxx
    """
    my_check_free_server_diskspace()
    my_check_input(HashValue, Blocksize, Usertoken)
    my_check_hash_array(hash_array)

    if len(hash_array) > MAX_BLOCKS:
        raise HTTPException(status_code=400, detail="too many blocks")

    username = my_check_usertoken(Usertoken)

    # Wenn der TOC schon existiert, überschreibe ich ihn, denn
    # des authentisierten Nutzers Wunsch ist mein Befehl.
    target_dir = f"{DATA_DIR}/{username}/{HashValue}/{Blocksize}"
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    with open(target_dir + "/TOC", "wt") as toc_file:
        toc_file.write(json.dumps(hash_array))

    return "OK"

@app.get("/HashesOfMissingBlocks/{HashValue}/{Blocksize}", tags=["HashesOfMissingBlocks"])
def get_hashes_of_missing_blocks(
        HashValue: str,
        Blocksize: int,
        Usertoken: Optional[str] = Header("anon")):
    """
    Gibt ein Array von Hashwerte von fehlenden Datenblöcken zurück.
    Falls das TOC noch nicht existiert, gibt er [ "all" ] zurück.
    Dann muss der Client ebenfalls das TOC hochladen.
    """

    my_check_input(HashValue, Blocksize, Usertoken)
    username = my_check_usertoken(Usertoken)

    target_toc = f"{DATA_DIR}/{username}/{HashValue}/{Blocksize}/TOC"
    if not os.path.exists(target_toc):
        # xxx evtl. protokollieren, der Client hat wahrscheinlich 
        # das Protokoll nicht eingehalten.
        return ["all"]

    with open(target_toc, "rt") as toc_file:
        mydata = toc_file.read()
        # xxx sollte man um das loads ein "try:" machen?
        myarray = json.loads(mydata)

    target_dir = f"{DATA_DIR}/{username}/{HashValue}/{Blocksize}/"

    still_missing_blocks = []
    for hashblock in myarray:
        myfiles = glob.glob(target_dir + hashblock + ".*")
        if not myfiles:
            still_missing_blocks.append(hashblock)

    return still_missing_blocks


def my_check_blockhash(hashvalue_to_check: str):
    """
    Ein Hashwert muss der Form:
        faa9153bd4e9709f47a12c708a97fa7e784130591c9ce5367ff2cd59fbf700fd
    sein.

    """
    if not re.match(r"[0-9a-f]{64}", hashvalue_to_check):
        raise HTTPException(status_code=400, detail="not valid hash value")

    return True

@app.put("/PushBlock/{HashValue}/{Blocksize}/{BlockHash}", tags=["PushBlock"])
async def put_block(
        HashValue: str,
        Blocksize: int,
        BlockHash: str,
        myrequest: Request,
        Usertoken: Optional[str] = Header("anon")):
    """
    xxx
    """

    my_check_free_server_diskspace()
    my_check_input(HashValue, Blocksize, Usertoken)
    my_check_blockhash(BlockHash)
    username = my_check_usertoken(Usertoken)

    target_dir = f"{DATA_DIR}/{username}/{HashValue}/{Blocksize}"
    if not os.path.exists(target_dir):
        # xxx logging, das sollte eigentlich nicht passieren,
        # passiert nur wenn der client sich nicht ans Protokoll
        # gehalten hat.
        os.makedirs(target_dir)

    myrandom = secrets.token_hex(16)

    if myrequest.headers['Content-Type'] != 'application/octet-stream':
        raise HTTPException(status_code=400, detail="expecting application/octet-stream")

    mydata = await myrequest.body()

    if len(mydata) == 0 or len(mydata) > MAX_BLOCK_SIZE:
        raise HTTPException(status_code=400, detail="upload block size must be in [1, 2^22]")

    if hashlib.sha256(mydata).hexdigest() != BlockHash:
        raise HTTPException(status_code=400, detail="uploaded data has incorrect hash value")

    with open(f"{target_dir}/{BlockHash}.{myrandom}", "wb") as block_file:
        block_file.write(mydata)

    return "OK"


