#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, json, hashlib, requests
#sys.path.append( os.getcwd() )

DATA_FILE = "test-data.bin"

# Im Produkttivbetrieb würde man die Blockgröße auf 1 oder 2 MiB setzen
BLOCK_SIZE = 1024

# Willkürkliche Festlegung, IMHO muss man ein Limit festlegen um den Server zu
# schützen (das setzt natürlich der Server dann ebenfalls mittels Vorabprüfung 
# durch).
MAX_BLOCKS = 500

# Das muss man später nutzerkonfigurierbar per config-Datei im JSON-Format
# machen.
SERVER_URL = "http://127.0.0.1:8000/"
#SERVER_URL = "http://127.0.0.1:8000/raspi/"


if not os.path.exists(DATA_FILE):
    sys.exit("Keine zu tranportierenden Daten gefunden.")

myfilesize = os.stat(DATA_FILE).st_size 
if myfilesize == 0:
    sys.exit("Filesize = 0 (?)")

if myfilesize // BLOCK_SIZE > MAX_BLOCKS - 1:
    sys.exit("Blocksize ist zu klein, oder die zu transportierende Daten sind zu groß")


with open(DATA_FILE, "rb") as f:

    HashOfData = hashlib.sha256()
    HasharrayOfBlocks = []
    OffsetByHash = dict()

    i = 0
    while mydata := f.read(BLOCK_SIZE):
        HashOfData.update(mydata)
        HasharrayOfBlocks.append(hashlib.sha256(mydata).hexdigest())
        OffsetByHash[HasharrayOfBlocks[-1]] = i*BLOCK_SIZE
        i += 1

    HashOfData = HashOfData.hexdigest()

    if len(sys.argv)>1:
        print(HashOfData, "=", HasharrayOfBlocks)
        sys.exit(0)

    HttpsSession = requests.Session()
    req = HttpsSession.get(SERVER_URL + "Status")

    # der test ist recht unspezifisch, xxx
    if req.status_code != requests.codes.ok:
        print("""
        # als erstes .*/Status pruefen
        # if FAIL
        #       https://de.wikipedia.org/wiki/Web_Proxy_Autodiscovery_Protocol
        #      -> http://wpad/wpad.dat laden -> Proxy konfigurieren
        #      if FAIL  
        #           Meldung an den Nutzer: Offline?
        """)
        sys.exit(1)


    # Request User_token (Challenge)
    # POST User_token Response & get User_token
    
    req = HttpsSession.get(
        f"{SERVER_URL}HashesOfMissingBlocks/{HashOfData}/{BLOCK_SIZE}")
    assert req.status_code == requests.codes.ok
    MissingBlocks = req.json()
    print(MissingBlocks) 

    while MissingBlocks:
        NextElement = MissingBlocks.pop(0)
        if NextElement == "all":
            req = HttpsSession.put( 
                    f"{SERVER_URL}TOC/{HashOfData}/{BLOCK_SIZE}",
                    data=json.dumps(HasharrayOfBlocks))
            assert req.status_code == requests.codes.ok
            print("Uploaded TOC:", HashOfData, "=", HasharrayOfBlocks)
            # hier kann es vorkommen, dass Blöcke in den Daten gleich sind
            # (bei verschlüsselten Daten ist das unmögich ... aber bei
            # unverschlüsseten Testdaten schon).
            # Deshalb mache ich hier quasi ein "unique" auf das array,
            # wobei ich aber die Reihenfolge erhalten möchte.
            # Deshalb geht ein MissingBlocks = [*{*HasharrayOfBlocks}] 
            # hier nicht.
            # Im Fall dass der Server mir die MissingBlocks sagt, macht
            # der Server schon das "unique".
            already_seen = set()
            for element in HasharrayOfBlocks:
                if not element in already_seen:
                    already_seen.add(element)
                    MissingBlocks.append(element)
        else:
            if not NextElement in OffsetByHash:
                # hier könnte man den TOC nochmal neu beim Server hochladen
                # dieser Fehlerfall sollte eigentlich nicht vorkommen
                sys.exit("der Datenblock existiert bei mir nicht; TOC auf dem Server falsch?")

            f.seek(OffsetByHash[NextElement])
            myblock = f.read(BLOCK_SIZE)
            req = HttpsSession.put( 
                    f"{SERVER_URL}PushBlock/{HashOfData}/{BLOCK_SIZE}/{NextElement}",
                    data=myblock,
                    headers={'Content-Type': 'application/octet-stream'})
            if req.status_code == requests.codes.ok:
                print("Block", NextElement, "hochgeladen")
            else:
                print("Fehler", req.status_code)
            
