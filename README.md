# raspi #

Kleines Spiel- und Lernprojekt: große Dateien per HTTPS auf einen Server
transferieren. Dazu werden die Dateien in Blöcke zerteilt und zunächst
die Hashwerte dieser Blöcke als Inhaltsverzeichnis hochgeladen.
Der Client fragt den Server, welche der Blöcke noch fehlen und lädt diese
fehlenden Blöcke beim Server hoch.

Damit kann man quasi beliebig den Upload Stoppen und Fortsetzen.

Es gibt eine Nutzerauthentsierung und eine Namespace-Separation pro
Nutzer in Bezug auf die Uploads.

Der Client verschlüsselt und signiert die Uploads, d. h. auf dem 
Upload-Server liegen nur Chiffrate (Senkung des Schutzbedarfes).

## Starten des Servers

0) Auf dem Server ggf. notwendige und noch fehlende python-Bibliotheken
   installieren

        pip install -r requirements.txt

1) via uvicron oder hypercorn starten

        uvicorn raspi:app --reload

   oder 

       ./start_server.sh

## Client starten

Testdaten erzeugen:
        
        $ ./gen-test-data.sh
        $ ls -l test-data.bin
        -rw-r--r-- 1 ... 10240 May 14 10:43 test-data.bin
        $ sha256sum.exe test-data.bin
        5190acb2dfb1a23fb13632d83ab2184b8f67464b7358423b990d8b7c96ed64f4 *test-data.bin

Informationen über die Datenzerlegung (im Client ist die Blockgröße 1KiB 
aus Testzwecken konfiguriert) ausgeben lassen:

        $ ./client.py info
        5190acb2dfb1a23fb13632d83ab2184b8f67464b7358423b990d8b7c96ed64f4 = ['2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f', '249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629', 'a4218a8faa56fd70c5228c6e94e7f524cf64166ae3754cf8201773399469956e', 'c0df3174c2dd9f4e68ad5de172fe8ab18d05ae725cdfff4a5d014ed43e8e30fb', '03b6dc5382759f153e225e15362c4c4e91e7b495eb612128cef390e801941103', '9e26cefc3c9d22482ea575645b3d5b6ada75c0f195706addf153d673be8f74a7', '2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7', 'd6715d10a872c707ab77686ad51fb8ea8d3921f7e7d5ff6186924a61d8cf8172', '4b97da7ea89364f44f504040152b2790e7e80ff80bb1ecaada211239c054e402', 'cb14b6a148981802f8df905715d07fb998e8a38daba6045761b1dc7674457532']

'all' (s. u.) heißt, dass dem Server alles zu der Datei (TOC) fehlt.

    $ ./client.py
    ['all']
    Uploaded TOC: 5190acb2dfb1a23fb13632d83ab2184b8f67464b7358423b990d8b7c96ed64f4 = ['2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f', '249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629', 'a4218a8faa56fd70c5228c6e94e7f524cf64166ae3754cf8201773399469956e', 'c0df3174c2dd9f4e68ad5de172fe8ab18d05ae725cdfff4a5d014ed43e8e30fb', '03b6dc5382759f153e225e15362c4c4e91e7b495eb612128cef390e801941103', '9e26cefc3c9d22482ea575645b3d5b6ada75c0f195706addf153d673be8f74a7', '2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7', 'd6715d10a872c707ab77686ad51fb8ea8d3921f7e7d5ff6186924a61d8cf8172', '4b97da7ea89364f44f504040152b2790e7e80ff80bb1ecaada211239c054e402', 'cb14b6a148981802f8df905715d07fb998e8a38daba6045761b1dc7674457532']
    Block 2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f hochgeladen
    Block 249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629 hochgeladen
    Block a4218a8faa56fd70c5228c6e94e7f524cf64166ae3754cf8201773399469956e hochgeladen
    Block c0df3174c2dd9f4e68ad5de172fe8ab18d05ae725cdfff4a5d014ed43e8e30fb hochgeladen
    Block 03b6dc5382759f153e225e15362c4c4e91e7b495eb612128cef390e801941103 hochgeladen
    Block 9e26cefc3c9d22482ea575645b3d5b6ada75c0f195706addf153d673be8f74a7 hochgeladen
    Block 2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7 hochgeladen
    Block d6715d10a872c707ab77686ad51fb8ea8d3921f7e7d5ff6186924a61d8cf8172 hochgeladen
    Block 4b97da7ea89364f44f504040152b2790e7e80ff80bb1ecaada211239c054e402 hochgeladen
    Block cb14b6a148981802f8df905715d07fb998e8a38daba6045761b1dc7674457532 hochgeladen

Lösche zum Test im Server die drei Blöcke 2e65…, 2490…, 2543…

    $ d 2
    rm 249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629.5d1f242269e42c75f8f9cf8c830a8e92
    rm 2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7.b7e426961d3dd6c322fcbf92479d307d
    rm 2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f.1184002a18f3b1e7387ed2b4a0de0ab2

Jetzt neu Hochladen … es werden nur die fehlenden Blöcke hochgeladen. Der
Server sagt es fehlen folgend in der Liste/Array aufgeführten Blöcke (durch den
Hashwert identiziert/adressiert) anschließend lädt der Client genau die drei
Blöcke hoch.

    $ ./client.py
    ['2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f', '249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629', '2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7']
    Block 2e65c6bca14031c3aa21876aa21ceb7b7fb98da5ac922ad509fa56f96f9b371f hochgeladen
    Block 249b02fa495a58115ac39f8e13a200d1871b1f4303048c9378ff6e7841f35629 hochgeladen
    Block 2543f18450dccc5cc2d7578fb84c7f78c1d6f0fd9a733978a24f07ba513537c7 hochgeladen

## Nutzerverwaltung / Sicherheitsbereiche

Die Nutzer bekommen eigene ECDSA-Schlüssel. Damit können sie sich
authentisieren und sie können den Upload signieren.

Der Upload wird zunächst fachlich im Client geprüft.
Schlägt dies fehl, so gibt es eine Rückmeldung an den Nutzer und Abbruch.
Anderenfalls werden die Daten
- Signiert mit dem privaten Nutzerschlüssel
- Das wird dann für den Sicherheitsbereich verschlüsselt.
- Das Chiffrat wird dann in BLOCK\_SIZE großen Blöcken hochgeladen.

Der Server kann also nicht den Klartext der Daten sehen.
Periodisch werden die Chiffrate vom Server-Admin in den Sicherheitsbereich
gebracht (bspw. per USB-Stick). Im Sicherheitsbereich liegt der private
Schlüssel vor, womit die Daten entschlüsselt werden kann.
Bei den Klartextdaten, kann dann die Signatur geprüft werden.


